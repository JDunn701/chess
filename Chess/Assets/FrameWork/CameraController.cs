using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController :MonoBehaviour
{ 
    public AnimationCurve curve;
    public float speed;
    public Transform[] targets;
  
    Vector3 nextPos;
    GridCreator instance;
    public void Awake()
    {
        instance = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GridCreator>();
        nextPos = targets[2].transform.position;

        StartCoroutine(MoveToPosition(nextPos, speed));

    }
 
    IEnumerator MoveToPosition( Vector3 targetTransform, float duration)
    {
       
        Debug.Log("Should be moving");
        float elapsedTime = 0.0f;

        while (elapsedTime < duration)
        {
            elapsedTime += Time.deltaTime;
            float fractionOfJourney = elapsedTime / duration;
            fractionOfJourney = curve.Evaluate(fractionOfJourney);
         
            transform.position = Vector3.Lerp(transform.position, targetTransform, fractionOfJourney);

            yield return null;
        }
        transform.position = targetTransform;
      
    }
}
