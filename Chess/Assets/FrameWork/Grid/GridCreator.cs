using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCreator : MonoBehaviour
{


    [Header("Art Assets")]
    [SerializeField]
    Material tileMaterial;
    [SerializeField]
    float dragOffset =2.0f;

    [Header("Prefabs and materials")]
    [SerializeField]
    private GameObject[] Prefabs;
    [SerializeField]
    private Material[] teamMaterials;

    //Logic
    ChessPiece[,] chessPieces;
    private ChessPiece currentChessPiece;
    private List<Vector2Int> availableMoves = new List<Vector2Int>();
    private List<ChessPiece> deadWhiteTeam = new List<ChessPiece>();
    private List<ChessPiece> deadBlackTeam = new List<ChessPiece>();
    const int tileCountX = 8; const int tileCountY = 8;

    [SerializeField]
    float tileSize = 1.0f;

    private GameObject[,] tiles;

    Camera currentCamera;
    private Vector2Int currentHoverInfo;

    [Header ("camera lerp fuctionality")]
    public AnimationCurve curve;
    public float speed;
    public Transform[] targets;
    public Transform camTarget;

    public bool isWhiteTeamsTurn;
    void Awake()
    {
        currentCamera = Camera.main;
      
        isWhiteTeamsTurn = true;
        BuildTiles(tileSize, tileCountX, tileCountY);
        SpawnAllPieces();
        PositionAllPieces();
    }
    void Update()
    {
      
        if (!currentCamera)
        {
            currentCamera = Camera.main;
            return;
        }

        RaycastHit info;
        Ray ray = currentCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out info, 100, LayerMask.GetMask("Tile", "Hover","Highlight")))
        {
            Vector2Int hitInfo = LookUpTileIndex(info.transform.gameObject);

            if (currentHoverInfo == -Vector2Int.one)
            {
                currentHoverInfo = hitInfo;
                tiles[hitInfo.x, hitInfo.y].layer =LayerMask.NameToLayer("Hover");
            }
            if (currentHoverInfo != hitInfo)
            {
                tiles[currentHoverInfo.x, currentHoverInfo.y].layer = (ContainsValidMove(ref availableMoves, currentHoverInfo)) ? LayerMask.NameToLayer("Highlight") : LayerMask.NameToLayer("Tile");
                currentHoverInfo = hitInfo;
                tiles[hitInfo.x, hitInfo.y].layer = LayerMask.NameToLayer("Hover");
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (chessPieces[hitInfo.x, hitInfo.y] != null)
                {
                    //check current players turn
                    if ((chessPieces[hitInfo.x,hitInfo.y].team==0 &&isWhiteTeamsTurn)|| (chessPieces[hitInfo.x, hitInfo.y].team == 1 && !isWhiteTeamsTurn))
                    {
                        currentChessPiece = chessPieces[hitInfo.x, hitInfo.y];
                        // get a list of available positions/ highlight tiles as well
                        availableMoves = currentChessPiece.GetAvailableMoves(ref chessPieces,tileCountX,tileCountY);

                        HighlightTiles();
                    }
                }
            }
            if (currentChessPiece != null && Input.GetMouseButtonUp(0))
            {
                Vector2Int previousPosition = new Vector2Int(currentChessPiece.currentX, currentChessPiece.currentY);
                bool validMove = MoveTo(currentChessPiece, hitInfo.x, hitInfo.y);

              

                if (!validMove)             
                    currentChessPiece.SetPosition(GetTileCentre(previousPosition.x, previousPosition.y));
            
                    currentChessPiece = null;
                     RemoveHighlightedTiles();
            }

        }
        else
        {
            if (currentHoverInfo != -Vector2Int.one)
            {
                tiles[currentHoverInfo.x, currentHoverInfo.y].layer = (ContainsValidMove(ref availableMoves,currentHoverInfo)) ? LayerMask.NameToLayer("Highlight"): LayerMask.NameToLayer("Tile");
                currentHoverInfo = -Vector2Int.one;
            }
            if(currentChessPiece && Input.GetMouseButtonUp(0))
            {
                currentChessPiece.SetPosition(GetTileCentre(currentChessPiece.currentX, currentChessPiece.currentY));
                currentChessPiece = null;
                RemoveHighlightedTiles();
            }

        }
        if(currentChessPiece)
        {
            Plane horizontalPlane = new Plane(Vector3.up, Vector3.up);
            float distance = 0.0f;
            if (horizontalPlane.Raycast(ray, out distance))
                currentChessPiece.SetPosition(ray.GetPoint(distance)+Vector3.up * dragOffset);

        }


    }

    //Generating the board
    void BuildTiles(float tileSize, int tileCountX, int tileCountY)
    {
        tiles = new GameObject[tileCountX, tileCountY];
        for (int x = 0; x < tileCountX; x++)
            for (int y = 0; y < tileCountY; y++)
                tiles[x, y] = GenerateAllTiles(tileSize, x, y);
    }
    GameObject GenerateAllTiles(float tileSize, int x, int y)
    {
        GameObject tileObject = new GameObject(string.Format("X:{0}, Y:{1}", x, y));
        tileObject.transform.parent = transform;

        Mesh mesh = new Mesh();
        tileObject.AddComponent<MeshFilter>().mesh = mesh;
        tileObject.AddComponent<MeshRenderer>();
        tileObject.GetComponent<MeshRenderer>().material = tileMaterial;

        Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(x * tileSize, 0, y * tileSize);
        vertices[1] = new Vector3(x * tileSize, 0, (y + 1) * tileSize);
        vertices[2] = new Vector3((x + 1) * tileSize, 0, y * tileSize);
        vertices[3] = new Vector3((x + 1) * tileSize, 0, (y + 1) * tileSize);
        int[] tris = new int[] { 0, 1, 2, 1, 3, 2 };

        mesh.vertices = vertices;
        mesh.triangles = tris;

        mesh.RecalculateNormals();
        tileObject.AddComponent<BoxCollider>();
        tileObject.layer = LayerMask.NameToLayer("Tile");


        return tileObject;
    }
    //Spawning of the pieces
    private void SpawnAllPieces()
    {
        chessPieces = new ChessPiece[tileCountX, tileCountY];
        int whiteTeam = 0; int blackTeam = 1;

        //White team
        chessPieces[0, 0] = SpawnSinglePiece(ChessPieceType.Rook, whiteTeam);
        chessPieces[1, 0] = SpawnSinglePiece(ChessPieceType.Knight, whiteTeam);
        chessPieces[2, 0] = SpawnSinglePiece(ChessPieceType.Bishop, whiteTeam);
        chessPieces[3, 0] = SpawnSinglePiece(ChessPieceType.King, whiteTeam);
        chessPieces[4, 0] = SpawnSinglePiece(ChessPieceType.Queen, whiteTeam);
        chessPieces[5, 0] = SpawnSinglePiece(ChessPieceType.Bishop, whiteTeam);
        chessPieces[6, 0] = SpawnSinglePiece(ChessPieceType.Knight, whiteTeam);
        chessPieces[7, 0] = SpawnSinglePiece(ChessPieceType.Rook, whiteTeam);
        for (int i = 0; i < tileCountX; i++)
            chessPieces[i, 1] = SpawnSinglePiece(ChessPieceType.Pawn, whiteTeam);

        //Black team
        chessPieces[0, 7] = SpawnSinglePiece(ChessPieceType.Rook, blackTeam);
        chessPieces[1, 7] = SpawnSinglePiece(ChessPieceType.Knight, blackTeam);
        chessPieces[2, 7] = SpawnSinglePiece(ChessPieceType.Bishop, blackTeam);
        chessPieces[3, 7] = SpawnSinglePiece(ChessPieceType.King, blackTeam);
        chessPieces[4, 7] = SpawnSinglePiece(ChessPieceType.Queen, blackTeam);
        chessPieces[5, 7] = SpawnSinglePiece(ChessPieceType.Bishop, blackTeam);
        chessPieces[6, 7] = SpawnSinglePiece(ChessPieceType.Knight, blackTeam);
        chessPieces[7, 7] = SpawnSinglePiece(ChessPieceType.Rook, blackTeam);
        for (int i = 0; i < tileCountX; i++)
            chessPieces[i, 6] = SpawnSinglePiece(ChessPieceType.Pawn, blackTeam);
    }
    private ChessPiece SpawnSinglePiece(ChessPieceType type, int team)
    {
        ChessPiece cP = Instantiate(Prefabs[(int)type - 1], transform).GetComponent<ChessPiece>();
        cP.type = type;
        cP.team = team;
        cP.GetComponent<MeshRenderer>().material = teamMaterials[team];
        return cP;
    }
    //Position
    void PositionAllPieces()
    {
        for (int x = 0; x < tileCountX; x++)
            for (int y = 0; y < tileCountY; y++)
                if (chessPieces[x, y] != null)
                    PositionSinglePiece(x, y, true);
    }
    void PositionSinglePiece(int x, int y, bool force = false)
    {
        chessPieces[x, y].currentX = x;
        chessPieces[x, y].currentY = y;
        chessPieces[x, y].SetPosition(GetTileCentre(x, y), force);


    }
    private Vector3 GetTileCentre(int x, int y)
    {
        return new Vector3(x * tileSize, 0.5f, y * tileSize) + new Vector3(tileSize / 2, 0, tileSize / 2);
    }

    //Highlighting tiles

    void HighlightTiles()
    {
        for(int i =0;i<availableMoves.Count;i++)
        {
            tiles[availableMoves[i].x, availableMoves[i].y].layer = LayerMask.NameToLayer("Highlight");
        }
    }
    void RemoveHighlightedTiles()
    {
        for (int i = 0; i < availableMoves.Count; i++)
        {
            tiles[availableMoves[i].x, availableMoves[i].y].layer = LayerMask.NameToLayer("Tile");

           
        } 
        availableMoves.Clear();
    }
    //OPERATIONS
    private bool ContainsValidMove(ref List<Vector2Int> moves,Vector2 pos)
    {
        for(int i=0;i<moves.Count;i++)     
            if (moves[i].x == pos.x && moves[i].y == pos.y)
                return true;

        return false;
    }
    private Vector2Int LookUpTileIndex(GameObject hitInfo)
    {
        for (int x = 0; x < tileCountX; x++)
            for (int y = 0; y < tileCountY; y++)
                if (tiles[x, y] == hitInfo)
                {
                    return new Vector2Int(x, y);
                }
        return -Vector2Int.one;
    }
    private bool MoveTo(ChessPiece cP, int x, int y)
    {
        //is there another piece on the target position
        if (!ContainsValidMove(ref availableMoves, new Vector2(x, y)))
            return false;

        if (chessPieces[x, y] != null)
        {
            ChessPiece ocp = chessPieces[x, y];
            if (cP.team == ocp.team)
            {
                return false;
            }

            if(ocp.team ==0)
            {
                deadWhiteTeam.Add(currentChessPiece);
                ocp.SetPosition(new Vector3(8 * tileSize, 0, -1 * tileSize)
                        + new Vector3(tileSize/2,0.5f,tileSize/2)
                        +(Vector3.forward *1.2f)*deadWhiteTeam.Count);
            }
            else
            {
                deadBlackTeam.Add(currentChessPiece);
                ocp.SetPosition(new Vector3(-1 * tileSize, 0, 8 * tileSize)
                       + new Vector3(tileSize / 2, 0.5f, tileSize / 2)
                       + (Vector3.back * 1.2f) * deadBlackTeam.Count);
            }
        }
        Vector2Int previousPosition = new Vector2Int(cP.currentX, cP.currentY);
        chessPieces[x, y] = cP;
        chessPieces[previousPosition.x, previousPosition.y] = null;
        PositionSinglePiece(x, y);
      
        isWhiteTeamsTurn = !isWhiteTeamsTurn;
        return true;
    }
   
}
